package com.joofthan.watch;

public class Log {
    public static void watcher(String text){
        System.err.println("[Watcher] "+text);
    }

    public static void info(String s) {
        System.out.println(s);
    }

    public static void error(String text) {
        System.out.println("[Watcher] Error: "+text );
    }
}
