package com.joofthan.watch.watcher;

import com.joofthan.watch.Log;
import com.sun.nio.file.SensitivityWatchEventModifier;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;

public class Watcher {
    private static final TemporalAmount MIN_RUN_DELAY = Duration.ofSeconds(2);

    private final WatchService watchService;
    private final Runnable runnable;
    private final Path dir;
    private LocalDateTime lastChange = LocalDateTime.MIN;

    public Watcher(Path dir, Runnable runnable) throws IOException {
        watchService = FileSystems.getDefault().newWatchService();
        registerAll(dir, watchService);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    System.out.println();
                    Log.watcher("Unregistering dirs...");
                   unregisterAll(dir,watchService);
                   Log.watcher("Closing watchService...");
                   watchService.close();
                   Log.watcher("Shutdown complete");
                } catch (IOException e){
                    throw new RuntimeException(e);
                }
            }
        });
        this.dir = dir;
        this.runnable = runnable;
    }

    private void registerAll(final Path start, WatchService watchService) throws IOException {
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                dir.register(watchService, new WatchEvent.Kind[] {StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE}, SensitivityWatchEventModifier.HIGH);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private void unregisterAll(final Path start, WatchService watchService) throws IOException {
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                WatchKey key = dir.register(watchService, new WatchEvent.Kind[] {StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE}, SensitivityWatchEventModifier.HIGH);
                key.cancel();
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public void runBlocking() {
        try{
            WatchKey key;
            String text = "Watching: "+dir.toAbsolutePath();
            Log.watcher(text);
            while ((key = watchService.take()) != null) {
                if(LocalDateTime.now().isAfter(lastChange.plus(MIN_RUN_DELAY))){
                    runnable.run();
                    Log.watcher(text);
                }else{
                    //Not running deploy because last deploy was less than "MIN_RUN_DELAY" seconds ago.
                }
                key.pollEvents().clear(); //remove all pending events to prevent second deploy
                lastChange = LocalDateTime.now();
                key.reset();
            }
        }catch (InterruptedException e){
            Log.error("runBlocking interupted");
            throw new RuntimeException(e);
        }
    }
}
