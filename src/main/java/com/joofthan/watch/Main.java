package com.joofthan.watch;

import com.joofthan.watch.command.Command;
import com.joofthan.watch.glassfish.Glassfish;
import com.joofthan.watch.warproject.WarProject;
import com.joofthan.watch.watcher.Watcher;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class Main {

    static Map<String, Consumer<String>> map = new HashMap<>();

    public static void main(String[] args){
        map.put("glassfish", Glassfish::runCommand);
        map.put("create", WarProject::runCommand);

        String firstArg = args[0];
        int argLength = args.length;

        //System.out.println("Args Length: "+argLength);
        //System.out.println("First arg: "+firstArg);

        if(argLength == 1 && firstArg.contains(":") && map.containsKey(firstArg.split(":")[0])){
            map.get(firstArg.split(":")[0]).accept(args[0].split(":")[1]);
            return;
        }


        try {
            doMain(args);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static void doMain(String[] args) throws IOException {

        if(args.length != 2){
            System.err.println("Useage:\njava -jar watcher.jar src \"./watcher.sh\"\n");
        }
        if(args.length > 2){
            return;
        }

        String path;
        if(args.length == 0){
            Log.watcher("Using default path: src");
            path = "src";
        }else{
            path = args[0];
        }
        String command;
        if(args.length <= 1){
            Log.watcher("Using default command: ./watcher.sh");
            command = "./watcher.sh";
            File file = new File("watcher.sh");
            if(!file.exists()){
                Log.error("File \""+file.getAbsolutePath()+"\" does not exist.");
                return;
            }
        }else {
            command = args[1];
        }

        Watcher watcher = null;
        Command runnable = new Command(command);
        runnable.run();
        watcher = new Watcher(new File(path).toPath(), runnable);
        watcher.runBlocking();
    }
}
