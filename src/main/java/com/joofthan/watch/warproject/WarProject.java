package com.joofthan.watch.warproject;

import com.joofthan.watch.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WarProject {
    private static String PERSISTENCE_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
            "<persistence xmlns=\"https://jakarta.ee/xml/ns/persistence\"\n" +
            "             xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
            "             xsi:schemaLocation=\"https://jakarta.ee/xml/ns/persistence https://jakarta.ee/xml/ns/persistence/persistence_3_0.xsd\"\n" +
            "             version=\"3.0\">\n" +
            "    <persistence-unit name=\"ExampleDS\" transaction-type=\"JTA\">\n" +
            "        <properties>\n" +
            "            <property name=\"jakarta.persistence.schema-generation.database.action\" value=\"drop-and-create\"/>\n" +
            "        </properties>\n" +
            "    </persistence-unit>\n" +
            "</persistence>\n";

    public static void runCommand(String subCommand) {
        try {
            doRun(subCommand);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void doRun(String subCommand) throws IOException {
        if("persistence".equals(subCommand)){
            File metaInf = new File("src/main/resources/META-INF");
            File pers = new File("src/main/resources/META-INF/persistence.xml");
            if(pers.exists()){
                Log.watcher(pers.getAbsolutePath() + " does already exist.");
                return;
            }
            metaInf.mkdirs();
            pers.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(pers));
            writer.write(PERSISTENCE_XML);
            writer.close();
            Log.info("Created: "+pers.getAbsolutePath());
        }
    }
}
