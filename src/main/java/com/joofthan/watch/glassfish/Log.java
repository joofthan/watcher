package com.joofthan.watch.glassfish;

public class Log {

    private static boolean debug = false;

    public static void info(String s) {
        com.joofthan.watch.Log.watcher(s);
    }

    public static void debug(String s) {
        if(debug)info(s);
    }
}
