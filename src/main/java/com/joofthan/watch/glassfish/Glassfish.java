package com.joofthan.watch.glassfish;

import com.joofthan.watch.command.Command;
import com.joofthan.watch.watcher.Watcher;

import java.io.File;
import java.io.IOException;

public class Glassfish {

    private final String asadminPath;
    private final String deployPath;
    private String version;

    public Glassfish(String version) {
        this.version = version;
        String mayor = version.substring(0,1);
        asadminPath = "target/glassfish/glassfish" + mayor + "/bin/asadmin";
        deployPath = "target/glassfish/glassfish" + mayor + "/glassfish/domains/domain1/autodeploy/";
    }




    public static void runCommand(String version) {
        Glassfish glassfish = new Glassfish(version);
        if(!glassfish.exists()){
            glassfish.download();
        }
        glassfish.start();
    }

    private boolean exists() {
        return new File("target", "glassfish.zip").exists();
    }
    private void download() {
        File targetZip = new File("target", "glassfish.zip");
        File targetFolder = new File("target", "glassfish");
        if(targetZip.exists()){
            throw new RuntimeException("glassfish does already exist");
        }
        targetZip.mkdirs();
        String url = "https://www.eclipse.org/downloads/download.php?file=/ee4j/glassfish/glassfish-" + version + ".zip";
        Downloader downloader = new Downloader(url);
        Log.info("Downloading: Glassfish Runtime "+version);
        Log.debug("Downloading: "+url);
        downloader.saveTo(targetZip);
        Log.debug("File saved to: "+targetZip.getAbsolutePath());

        targetFolder.mkdirs();
        UnZipper unzipper = new UnZipper(targetZip);
        Log.info("Extracting...");
        Log.debug("Extracting to: "+targetFolder.getAbsolutePath());
        unzipper.saveToFolder(targetFolder.toPath());
        new Command("chmod +x "+ asadminPath).run();
        Log.info("Installation finished");
    }

    private void start() {
        Log.info("Starting...");
        new Command("./"+ asadminPath +" start-domain").run();
        Log.info("Starting Database...");
        new Command("./"+ asadminPath +" start-database").run();
        Log.info("Running on: http://localhost:8080/");
        Log.info("View Log: http://localhost:4848/common/logViewer/logViewerRaw.jsf?instanceName=server");

        try {
            new Watcher(new File("src").toPath(), new Runnable() {
                @Override
                public void run() {
                    new Command("mvn package").run();
                    File di = new File("target");
                    String[] all = di.list();
                    boolean isDeployed = false;
                    for (int i = 0; i < all.length; i++) {
                        String name = all[i];
                        if(name.endsWith(".war")){
                            if(isDeployed){
                                Log.info("Error: More than 1 war found.");
                                return;
                            }
                            new Command("./"+asadminPath+" deploy --force --name myApplication --contextroot / target/"+name).run();
                            isDeployed =true;
                        }
                    }
                }
            }).runBlocking();
        } catch (IOException e) {
            throw new RuntimeException();
        }


        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                    Log.info("Stopping Database...");
                    new Command("./"+ asadminPath +" stop-database").run();
                    Log.info("Stopping Runtime...");
                    new Command("./"+ asadminPath +" stop-domain").run();
            }
        });
    }
}
