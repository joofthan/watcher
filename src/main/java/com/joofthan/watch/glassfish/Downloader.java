package com.joofthan.watch.glassfish;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class Downloader {

    private String url;

    public Downloader(String url) {

        this.url = url;
    }

    void saveTo(File target){
        try {
            InputStream in = new URL(url).openStream();
            Files.copy(in, target.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
