package com.joofthan.watch.command;

import com.joofthan.watch.Log;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;

public class Command implements Runnable {

    private String command;

    public Command(String command) {
        this.command = command;
    }

    @Override
    public void run(){
        try {
            String[] commands = command.split("&&");
            Log.watcher("Running command: "+ command);
            LocalDateTime start = LocalDateTime.now();
            for (int i = 0; i < commands.length; i++) {
                doRun(commands[i]);
            }
            Duration d = Duration.between(start, LocalDateTime.now());
            if(d.toMillis() >= 5000){
                Log.watcher("finished in: "+(d.toMillis()*100) +" Seconds");
            }else{
                Log.watcher("finished in: "+d.toMillis() + " ms");
            }

        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    private void doRun(String command) throws IOException, InterruptedException {
        Process r = Runtime.getRuntime().exec(command);
        StreamGobbler contentCollector = new StreamGobbler(r.getInputStream());
        Executors.newSingleThreadExecutor().submit(contentCollector);
        Executors.newSingleThreadExecutor().submit(contentCollector);
        int exitCode = r.waitFor();
        if(exitCode != 0){
            Log.info(contentCollector.content);
        }
        assert exitCode == 0;
    }
}
