package com.joofthan.watch.command;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

class StreamGobbler implements Runnable {
    private InputStream inputStream;
    public String content ="";

    public StreamGobbler(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public void run() {
        new BufferedReader(new InputStreamReader(inputStream)).lines()
                .forEach((e)->add(e));
    }

    private void add(String e) {
        content = content + "\n"+e;
    }
}
