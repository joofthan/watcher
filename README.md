# Watcher
Watcher watches a specifies folder for changes. On every file change it executes the given command.
It uses the build in filesystem change events when available on the system.
## Build
`mvn package`

## Useage
`java -jar watcher.jar [PATH_TO_WATCH] [COMMAND_TO_EXECUTE]`
### Default
`java -jar watcher.jar`
- Watches default folder `src`
- executes default command `./watcher.sh`

### Specific folder (1 Param)
`java -jar watcher.jar myFolder`
- Watches folder `myFolder`
- executes file `./watcher.sh`

### Custom (2 Params)
`java -jar watcher.jar myFolder "mvn package -Dmaven.test.skip"`
- Watches folder `myFolder`
- executes command `mvn package -Dmaven.test.skip`


---

## Advanced
### Run
The following command will build and run the project from the current directory. 

`java -jar watcher.jar glassfish:6.2.3`
- Watches folder `src`
- Redeploy on change

### Create
`java -jar watcher.jar create:persistence`

creates a persistence.xml file in directory: `src/main/resources/META-INF/`